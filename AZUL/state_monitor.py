from collections import deque, defaultdict

states = deque(maxlen=1)
search_time = deque(maxlen=1)


def set_state(state):
    states.append(state)


def get_state():
    return states[0]


# def set_time(time):
#     search_time.append(time)
#
# def get_time():
#     return search_time[0]
times = defaultdict(float)


def set_search_time(name, time):
    times[name] += time


def get_search_time():
    return times


speical_actions = defaultdict(int)


def increase_actions(name):
    speical_actions[name] += 1


def get_special_action():
    action_tuple = sorted(speical_actions.items(), key=lambda x:x[1], reverse=True)
    return action_tuple


round_recorder = []


def set_round_recorder(scores):
    if len(round_recorder) == 0 or len(round_recorder[-1]) == 6:
        if len(round_recorder) % 2 == 1:
            round_recorder.append([[scores[1], scores[0]]])
        else:
            round_recorder.append([scores])
    else:
        if len(round_recorder) % 2 == 0:
            round_recorder[-1].append([scores[1], scores[0]])
        else:
            round_recorder[-1].append(scores)


def get_round_recorder():
    string = ''
    for i in range(len(round_recorder)):
        for scrs in round_recorder[i]:
            string += '[{:>5},{:>5}]'.format(str(scrs[0]), str(scrs[1]))
        string += '\n'
        if i % 2 == 1:
            string += '\n'
    return string


predict_recorder = {}


def set_pre_r(name, pre_score_list):
    if predict_recorder.get(name) is None:
        predict_recorder[name] = [pre_score_list]
    else:
        predict_recorder[name].append(pre_score_list)


def get_pre_r():
    string = ''
    for n, l in predict_recorder.items():
        string += (n + str(l) + '\n')
    return string
