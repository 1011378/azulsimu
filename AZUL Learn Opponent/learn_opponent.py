from __future__ import absolute_import, division, print_function, unicode_literals

import random

from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt


class Net_model:


    def train(self, x, y):
        model = keras.models.Sequential([
            keras.layers.Flatten(input_shape=(150, 6)),
            keras.layers.Dense(1024, activation='relu'),
            # keras.layers.Dropout(0.2),
            keras.layers.Dense(150, activation='softmax')
        ])

        model.compile(optimizer='adam',
                      loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])
        train_num = len(x)//5*4
        print('tn', train_num)
        DATA_X = np.array(x)
        DATA_Y = np.array(y)

        TR_X, TE_X = DATA_X[:train_num], DATA_X[train_num:]
        TR_Y, TE_Y = DATA_Y[:train_num], DATA_Y[train_num:]

        cost = model.fit(TR_X, TR_Y, epochs=50)
        print(cost)
        cost = model.evaluate(TE_X, TE_Y)
        print(cost)
        Y_pred = model.predict(TE_X)

        # print(TE_X[0])
        # print(np.argmax(Y_pred[0]))
        model.save('naive_model.h5')

    def perdict(self, X):
        model = keras.models.load_model('naive_model.h5')
        Y_pred = model.predict(X)
        return Y_pred

